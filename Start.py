from ScreenShow import Screen
from SerialReader import Reader
import serial

S = Screen()

ser = serial.Serial('/dev/ttyUSB0', 9600, timeout=1)
ser.reset_input_buffer()
i = 0
while True:
    i = i + 1
    if ser.in_waiting > 0:
        line = ser.readline().decode('utf-8').rstrip()
        readline = str(line)
        print(readline)
        Screen.DrawText(S, readline)
    else:
        line = "No GPS data " + str(i)
        Screen.DrawText(S, line)
