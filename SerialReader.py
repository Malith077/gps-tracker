import serial

class Reader:
    def __init__(self):
        self.ser = serial.Serial('/dev/ttyUSB0', 9600, timeout=1)
        self.ser.reset_input_buffer()
    
    def getData(self):
        if self.ser.in_waiting > 0:
            return self.ser.readline().decode('utf-8').rstrip()            
