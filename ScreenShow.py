import board
import time
import busio
import adafruit_ssd1306
from PIL import Image, ImageDraw, ImageFont

class Screen:
    
    def __init__(self):
        i2c = busio.I2C(board.SCL,board.SDA)
        self.oled = adafruit_ssd1306.SSD1306_I2C(128, 64, i2c, addr=0x3c)
        self.oled.fill(0)
        self.oled.show()
        self.image = Image.new("1", (self.oled.width, self.oled.height))
        self.draw = ImageDraw.Draw(self.image)
        self.font = ImageFont.load_default()

    
    def DrawText(self, text):
        # i2c = busio.I2C(board.SCL,board.SDA)
        # self.oled = adafruit_ssd1306.SSD1306_I2C(128, 64, i2c, addr=0x3c)
        # self.oled.fill(0)
        self.oled.show()
        self.image = Image.new("1", (self.oled.width, self.oled.height))
        self.draw = ImageDraw.Draw(self.image)
        self.font = ImageFont.load_default()
        # self.oled.fill(0)
        self.oled.show()
        time.sleep(1)
        (font_width, font_height) = self.font.getsize(text)
        self.draw.text((self.oled.width // 2 - font_width // 2, self.oled.height // 2 - font_height // 2),text, font=self.font, fill=255,)
        self.oled.image(self.image)
        self.oled.show()
        time.sleep(1)
        # self.oled.fill(0)

        